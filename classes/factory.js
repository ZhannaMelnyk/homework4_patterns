export default class MessageFactory {
  constructor(roomId, roomsMap) {
    this.roomId = roomId;
    this.roomsMap = roomsMap
  }

  create(type) {
    let messageCreator;
    if (type === 'gameEvent') {
      messageCreator = new GameEventMessageCreator(this.roomId, this.roomsMap)
    } else if (type === 'dummy') {
      messageCreator = new DummyMessageCreator()
    }

    messageCreator.getMessage = function () {
      return this.currentMessage;
    }

    return messageCreator;
  }
}

class GameEventMessageCreator {
  constructor(roomId, roomsMap) {
    this.currentMessage = roomsMap.get(roomId).systemMessagesQueue.shift()
  }
}

class DummyMessageCreator {
  constructor() {
    this.currentMessage = dummyQueue[Math.round(Math.random() * 9)];
  }
}

const dummyQueue = [
  'Врум врум врум',
  'Оце шумахер...',
  'Жулік, користуй десятипальцевий набір!',
  'Формула 1 відпочиває...',
  'Йойки, із клавіатури дим пішов!',
  'Здається, перший учасник під допінгом',
  'Яка боротьба!',
  'Нагадую, попкорн можна купити за рогом',
  'Заміна, у другого гравця вивих пальця!',
  'Як бачите, через карантин усі гонщики в масках...',
  'Покажи все, на що здатний!'
]
