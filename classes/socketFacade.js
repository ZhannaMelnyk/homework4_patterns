export default class socketFacade {
  constructor(io, socket) {
    this.io = io;
    this.socket = socket;
  }

  createNewHandlerForEvent(eventName, handler) {
    this.socket.on(eventName, handler);
  }

  join(roomId, callback) {
    this.socket.join(roomId, callback);
  }

  leave(roomId, callback) {
    this.socket.leave(roomId, callback);
  }

  sendOnlyToCurrentClient(eventName, params) {
    this.socket.emit(eventName, ...params);
  }

  sendToAll(eventName, params) {
    this.io.emit(eventName, ...params);
  }

  sendToAllClientsExceptCurrentClient(eventName, params) {
    this.socket.broadcast.emit(eventName, ...params);
  }

  sendToAllInRoom(roomId, eventName, params) {
    this.io.to(roomId).emit(eventName, ...params);
  }

  sendToAllInRoomExceptCurrentClient(roomId, eventName, params) {
    this.socket.to(roomId).emit(eventName, ...params);
  }
}