import MessageFactory from './factory';
import { compareForLeaderBoard } from '../helpers/compare';

export default class Bot {
  constructor(gamePlayers) {
    this.gamePlayers = gamePlayers;
  }

  getHelloMessage() {
    return `Hello!
    My name is Bot
    And now I will comment this game`
  }

  getPlayersAnnouncementMessage() {
    let counter = 1;
    const introduction = `Our players:
    `
    const players = this.gamePlayers.map(player => {
      return `${counter++}. ${player.name};
      `
    })

    const ending = `
    The game will start in few seconds.
    Good luck to all players :)
    `

    return introduction.concat(players.join(''), ending);
  }

  getGameMessages(roomId, roomsMap) {
    const messageFactory = new MessageFactory(roomId, roomsMap);
    let messageCreator;

    if (roomsMap.get(roomId).systemMessagesQueue.length > 0) {
      messageCreator = messageFactory.create('gameEvent');
    } else {
      messageCreator = messageFactory.create('dummy');
    }
    return messageCreator.getMessage();
  }

  getLeaderBoardMessage(players) {
    players = players.map(player => {
      if (player.winnerTablePosition == 0) {
        player.winnerTablePosition = 6;
      }
      return player;
    })
    const leaders = players.sort(compareForLeaderBoard).slice(0, 3);

    let counter = 1

    const textForLeaders = leaders.map(leader => {
      return `${counter++}. ${leader.name}
`
    })

    return textForLeaders.join('');
  }
}

