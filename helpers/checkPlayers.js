export function checkIfAllPlayersReady(players) {
  for (let i = 0; i < players.length; i++) {
    if (!players[i].isReady) {
      return false
    }
  }
  return true
}

export function checkIfAllPlayersFinished(players) {
  for (let i = 0; i < players.length; i++) {
    if (players[i].winnerTablePosition === 0) {
      return false
    }
  }
  return true
}