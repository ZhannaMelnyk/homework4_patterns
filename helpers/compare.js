export function compareForLeaderBoard(a, b) {
  const playerOne = a.winnerTablePosition;
  const playerTwo = b.winnerTablePosition;

  let comparison = 0;
  if (playerOne > playerTwo) {
    comparison = 1;
  } else if (playerOne < playerTwo) {
    comparison = -1;
  }
  return comparison;
}

export function compareForGameStatus(a, b) {
  const playerOne = a.currentWordIndex;
  const playerTwo = b.currentWordIndex;

  let comparison = 0;
  if (playerOne < playerTwo) {
    comparison = 1;
  } else if (playerOne > playerTwo) {
    comparison = -1;
  }
  return comparison;
}