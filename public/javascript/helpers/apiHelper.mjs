import { getTextById } from '../services/textService.mjs'

export const getTextByIndex = async (roomId, textIndex, socket) => {
  let text;
  await getTextById(textIndex)
    .then(returnedText => {
      text = returnedText;
    });
  socket.emit('GET_TEXT_BY_ID_DONE', roomId, text)
}