import { createElement, removeClass, addClass } from './domHelper.mjs';
import { updateRoomsList, updatePlayersInGameRoom } from './updateHelper.mjs'

const lobbyPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');

export const changeToGameRoom = (room, socket) => {
  addClass(lobbyPage, 'display-none');
  removeClass(gamePage, 'display-none');

  gamePage.style.display = 'grid';

  const currentPlayer = room.players.find(player => player.socketID === socket.id);

  gamePage.innerHTML = "";

  const gameName = createElement({
    tagName: 'h2',
    className: 'game-name',
    innerText: room.name
  });

  const leaveRoomBtn = createElement({
    tagName: 'button',
    className: 'btn leave-room-btn',
    attributes: { id: 'leave-room-btn' },
    innerText: 'Back To Rooms'
  });

  leaveRoomBtn.addEventListener('click', () => {
    socket.emit('LEAVE_ROOM', room.name);
  });

  const playersContainer = updatePlayersInGameRoom(room.players, socket);

  const gameContainer = createElement({
    tagName: 'div',
    className: 'game-container',
    attributes: { id: `game-container` }
  });

  const botContainer = createElement({
    tagName: 'div',
    className: 'bot-container',
  })

  const botImg = createElement({
    tagName: 'img',
    className: 'bot-img',
    attributes: {
      src: '../assets/bot.png',
      alt: 'bot'
    }
  })

  const botMessageContainer = createElement({
    tagName: 'div',
    className: 'bot-message-container',
    attributes: { id: 'bot-message-container' }
  })

  const isReadyBtn = createElement({
    tagName: 'button',
    className: 'btn isReady',
    attributes: { id: 'isReady' },
    innerText: 'Ready'
  });

  const playerIsReady = () => {
    socket.emit('PLAYER_IS_READY', room.name, currentPlayer.name);
    isReadyBtn.removeEventListener('click', playerIsReady)
  }

  isReadyBtn.addEventListener('click', playerIsReady);

  const timerBeforeGame = createElement({
    tagName: 'h2',
    className: 'time-before-game',
    attributes: { id: `time-before-game` }
  })

  timerBeforeGame.style.display = 'none';

  const timerForGame = createElement({
    tagName: 'span',
    className: 'time-for-game',
    attributes: { id: 'time-for-game' }
  })

  timerForGame.style.display = 'none';

  const gameTextBlock = createElement({
    tagName: 'div',
    className: 'game-text-block',
    attributes: { id: `game-text-block` }
  })

  gameTextBlock.style.display = 'none';

  gameContainer.append(isReadyBtn, timerBeforeGame, timerForGame, gameTextBlock);
  botContainer.append(botImg, botMessageContainer);
  gamePage.append(gameName, leaveRoomBtn, playersContainer, gameContainer, botContainer);
}

export const changeToLobby = (rooms, socket) => {
  addClass(gamePage, 'display-none');
  removeClass(lobbyPage, 'display-none');
  
  gamePage.style.display = 'none';

  updateRoomsList(rooms, socket);
}