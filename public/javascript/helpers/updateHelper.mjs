import { createElement } from './domHelper.mjs';
import { getFinishedLetters, getCurrentLetter, getNextLetters } from './textHelper.mjs'

const roomsContainer = document.getElementById('rooms-container');

export const updateRoomsList = (rooms, socket) => {
  roomsContainer.innerHTML = "";
  rooms.forEach(room => {
    const roomId = room.name;

    const roomContainer = createElement({
      tagName: 'div',
      className: 'room-preview',
    });

    const playerAmount = createElement({
      tagName: 'span',
      className: 'player-amount',
      innerText: `${room.players.length} users connected`
    });

    const roomName = createElement({
      tagName: 'h3',
      className: 'room-name',
      innerText: room.name
    });

    const joinRoomBtn = createElement({
      tagName: 'button',
      className: 'btn join-room-btn',
      innerText: 'Join Room'
    });

    joinRoomBtn.addEventListener('click', () => {
      socket.emit('JOIN_ROOM', roomId);
    });

    roomContainer.append(playerAmount, roomName, joinRoomBtn);
    roomsContainer.append(roomContainer);
  });
}

export const updatePlayersInGameRoom = (players, socket) => {
  const playersContainer = document.getElementById('players-container')
    ? document.getElementById('players-container')
    : createElement({
      tagName: 'div',
      className: 'players-container',
      attributes: { id: 'players-container' }
    });

  playersContainer.innerText = '';

  players.forEach(player => {
    const playerElement = createElement({
      tagName: 'div',
      className: 'player-container'
    })

    const readinessIndicator = createElement({
      tagName: 'div',
      className: 'readiness-indicator',
      attributes: {
        'id': `${player.name}-readiness-indicator`
      }
    });

    player.isReady
      ? readinessIndicator.style.backgroundColor = "green"
      : readinessIndicator.style.backgroundColor = "red"

    const playerName = createElement({
      tagName: 'h5',
      className: 'game-user-name',
      innerText: player.socketID === socket.id ? `${player.name} (you)` : player.name
    });

    const progressBlock = createElement({
      tagName: 'div',
      className: 'progress-block'
    });

    const progressIndicator = createElement({
      tagName: 'div',
      className: 'progress-indicator',
      attributes: {
        'id': `${player.name}-progress-indicator`
      }
    });

    progressBlock.append(progressIndicator);
    playerElement.append(readinessIndicator, playerName, progressBlock);
    playersContainer.append(playerElement);
  })

  return playersContainer;
}

export const updatePlayerInfo = (room, socket) => {
  const currentPlayer = room.players.find(player => player.socketID === socket.id);

  const readinessIndicator = document.getElementById(`${currentPlayer.name}-readiness-indicator`);
  const isReadyBtn = document.getElementById(`isReady`);

  const playerIsReady = () => {
    socket.emit('PLAYER_IS_READY', room.name, currentPlayer.name);
    isReadyBtn.removeEventListener('click', playerIsReady)
  }

  const playerIsNotReady = () => {
    socket.emit('PLAYER_IS_NOT_READY', room.name, currentPlayer.name);
    isReadyBtn.removeEventListener('click', playerIsNotReady)
  }

  if (currentPlayer.isReady) {
    readinessIndicator.style.backgroundColor = "green";
    isReadyBtn.innerText = 'Not Ready';
    isReadyBtn.addEventListener('click', playerIsNotReady);
  } else {
    readinessIndicator.style.backgroundColor = "red";
    isReadyBtn.innerText = 'Ready';
    isReadyBtn.addEventListener('click', playerIsReady);
  }
}

export const updateGameViewForStart = (roomId, socket) => {
  const isReadyBtn = document.getElementById('isReady');
  isReadyBtn.style.display = 'none';

  const leaveRoomBtn = document.getElementById('leave-room-btn');
  leaveRoomBtn.style.display = "none";

  socket.emit('START_TIMERS', roomId);
}

export const updateGameViewForNewGame = () => {
  const isReadyBtn = document.getElementById('isReady');
  isReadyBtn.style.display = 'block';
  isReadyBtn.innerText = 'Ready';

  const leaveRoomBtn = document.getElementById('leave-room-btn');
  leaveRoomBtn.style.display = 'block';

  const gameTextBlock = document.getElementById('game-text-block');
  gameTextBlock.style.display = 'none';
  gameTextBlock.innerText = '';

  const timerForGame = document.getElementById('time-for-game');
  timerForGame.style.display = 'block';
  timerForGame.innerText = '';
}

export const updateTimerBeforeGame = timeBeforeGame => {
  const timerBeforeGame = document.getElementById('time-before-game');
  timerBeforeGame.style.display = 'block';
  timerBeforeGame.innerText = '';
  timerBeforeGame.innerText = timeBeforeGame;
}

export const updateTimerForGame = gameTime => {
  const timerForGame = document.getElementById('time-for-game');
  timerForGame.style.display = 'block';
  timerForGame.innerText = '';
  timerForGame.innerText = `${gameTime} seconds left`;
}

export const updateTextInGameContainer = (text, player, roomId, socket) => {
  const timerBeforeGame = document.getElementById('time-before-game');
  timerBeforeGame.style.display = 'none';

  const gameTextBlock = document.getElementById('game-text-block');
  gameTextBlock.style.display = 'block';
  gameTextBlock.innerText = '';

  const finishedLetters = createElement({
    tagName: 'span',
    className: 'finished-letters',
    attributes: { id: 'finished-letters' },
    innerText: getFinishedLetters(text, player)
  })

  const currentLetter = createElement({
    tagName: 'span',
    className: 'current-letter',
    attributes: { id: 'current-letter' },
    innerText: getCurrentLetter(text, player)
  })

  const nextLetters = createElement({
    tagName: 'span',
    className: 'next-letters',
    attributes: { id: 'next-letters' },
    innerText: getNextLetters(text, player)
  })

  const checkLetters = (event) => {
    socket.emit('INPUT_LETTER', event.key, roomId);
    document.removeEventListener('keypress', checkLetters);
  }
  document.addEventListener('keypress', checkLetters)

  gameTextBlock.append(finishedLetters, currentLetter, nextLetters)
}

export const updatePlayersProgressIndicator = (text, player) => {
  const progressIndicator = document.getElementById(`${player.name}-progress-indicator`);

  progressIndicator.style.width = text.length - player.currentWordIndex > 0
    ? `${(player.currentWordIndex * 100) / text.length}%`
    : '100%'

  progressIndicator.style.width === '100%'
    ? progressIndicator.style.backgroundColor = 'green'
    : null
}

export const updateMessageFromBot = botMessage => {
  const botMessageContainer = document.getElementById('bot-message-container');

  botMessageContainer.innerText = '';
  botMessageContainer.innerText = botMessage
}