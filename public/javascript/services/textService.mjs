export const getTextById = async id => {
  return await fetch(`http://localhost:3002/game/texts/${id}`, { headers: { "Content-Type": "text/plain" } })
    .then(response => {
      return response.text()
    })
};