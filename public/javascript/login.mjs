const username = sessionStorage.getItem('username');

if (username) {
  window.location.replace('/game');
}

const socket = io('http://localhost:3002/login', { query: { username } });

const submitButton = document.getElementById('submit-button');
const input = document.getElementById('username-input');

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
  const inputValue = getInputValue();
  if (!inputValue) {
    return;
  }
  socket.emit('CHECK_USERNAME', inputValue);
  sessionStorage.setItem('username', inputValue);
  window.location.replace('/game');
};

const onKeyUp = ev => {
  const enterKeyCode = 13;
  if (ev.keyCode === enterKeyCode) {
    submitButton.click();
  }
};

submitButton.addEventListener('click', onClickSubmitButton);
window.addEventListener('keyup', onKeyUp);

const askUserToRelogin = username => {
  alert(`User with nickname "${username}" already exists`);
  sessionStorage.removeItem('username');
  window.location.replace('/login');
} 

socket.on('NEED_RELOGIN', askUserToRelogin);