// let's imagine that the usernames are stored not on the server, but somewhere in the database :)
import socketFacade from '../classes/socketFacade';

const usersSet = new Set();

let namesCache = [];

export default io => {
  io.on('connection', socket => {
    const username = socket.handshake.query.username;

    const customSocket = new socketFacade(io, socket);

    const checkUsername = username => {
      // Proxy pattern
      const checkUsernameProxy = new Proxy(usersSet, {
        has: function (target, prop) {
          if (namesCache.find(element => element === prop) !== undefined) {
            socket.emit('NEED_RELOGIN', prop);
          } else if (target.has(prop)) {
            socket.emit('NEED_RELOGIN', prop);
            namesCache.push(prop)
          } else {
            usersSet.add(prop);
            namesCache.push(prop);
          }
        }
      })
      username in checkUsernameProxy;
    }

    customSocket.createNewHandlerForEvent('CHECK_USERNAME', checkUsername)
  });
};