import * as config from './config';
import Bot from '../classes/bot';
import socketFacade from '../classes/socketFacade';
import { compareForGameStatus } from '../helpers/compare';
import { checkIfAllPlayersReady, checkIfAllPlayersFinished } from '../helpers/checkPlayers'

// let's imagine that the rooms are stored not on the server, but somewhere in the database :)
const roomsMap = new Map();

const roomNamesCache = [];

const getCurrentRoomsList = () => {
  let currentRooms = [];
  roomsMap.forEach(room => {
    const isRoomNeedToShow = room.players.length > 0
      && room.players.length < config.MAXIMUM_USERS_FOR_ONE_ROOM
      && !room.isTimerStart

    if (isRoomNeedToShow) {
      currentRooms.push(room)
    }
  });
  return currentRooms;
}

let winnerTablePosition = 0;

export default io => {
  io.on("connection", socket => {
    const customSocket = new socketFacade(io, socket);

    const username = socket.handshake.query.username;
    const currentRoomsList = getCurrentRoomsList();

    let currentRoom;
    customSocket.sendOnlyToCurrentClient('UPDATE_ROOMS', [currentRoomsList])

    customSocket.createNewHandlerForEvent('CREATE_NEW_ROOM', createNewRoom);
    customSocket.createNewHandlerForEvent('JOIN_ROOM', joinToRoom);
    customSocket.createNewHandlerForEvent('LEAVE_ROOM', leaveRoom);
    customSocket.createNewHandlerForEvent('PLAYER_IS_READY', setPlayerIsReady);
    customSocket.createNewHandlerForEvent('PLAYER_IS_NOT_READY', setPlayerIsNotReady);
    customSocket.createNewHandlerForEvent('GET_TEXT_BY_ID_DONE', getTextByIdIsDone);
    customSocket.createNewHandlerForEvent('INPUT_LETTER', checkInputLetter);

    function createNewRoom(name) {
      const checkRoomNameProxy = new Proxy(roomsMap, {
        has: function (target, prop) {
          if (roomNamesCache.find(element => element === prop) !== undefined) {
            customSocket.sendOnlyToCurrentClient('CREATE_NEW_ROOM_NOT_DONE', [prop])
            return false;
          } else if (target.has(prop)) {
            customSocket.sendOnlyToCurrentClient('CREATE_NEW_ROOM_NOT_DONE', [prop])
            roomNamesCache.push(prop);
            return false;
          } else {
            roomNamesCache.push(prop);
            return true;
          }
        }
      })
      if (name in checkRoomNameProxy) {
        roomsMap.set(name, {
          name,
          text: [],
          isTimerStart: false,
          isFinished: false,
          players: [],
          textIndex: -1,
          systemMessagesQueue: []
        })
        const currentRoomsList = getCurrentRoomsList();
        customSocket.sendToAllClientsExceptCurrentClient('UPDATE_ROOMS', [currentRoomsList]);
        customSocket.sendOnlyToCurrentClient('CREATE_NEW_ROOM_DONE', [name]);
      }
    }

    function joinToRoom(roomId) {
      currentRoom = roomsMap.get(roomId);

      currentRoom.players.push({
        socketID: socket.id,
        name: username,
        isReady: false,
        currentWordIndex: 0,
        winnerTablePosition
      });
      roomsMap.set(roomId, currentRoom);

      customSocket.join(roomId, joinToRoomForSocket);

      customSocket.sendToAllInRoomExceptCurrentClient(roomId, 'UPDATE_ROOM', [currentRoom.players]);

      const currentRoomsList = getCurrentRoomsList();

      customSocket.sendToAllClientsExceptCurrentClient('UPDATE_ROOMS', [currentRoomsList]);
    }

    function joinToRoomForSocket() {
      customSocket.sendOnlyToCurrentClient('JOIN_ROOM_DONE', [currentRoom]);
    }

    function leaveRoom(roomId) {
      const currentRoom = roomsMap.get(roomId);

      roomsMap.set(roomId, deleteUser(currentRoom));

      customSocket.leave(roomId, leaveToRoomForSocket)

      let currentRoomsList;

      function leaveToRoomForSocket() {
        currentRoomsList = getCurrentRoomsList();
        customSocket.sendOnlyToCurrentClient('LEAVE_ROOM_DONE', [currentRoomsList]);

        if (currentRoom.players.length > 0) {
          customSocket.sendToAllInRoom(roomId, 'UPDATE_ROOM', [currentRoom.players])

          const isGameCanStart = currentRoom.players.length > 1 && checkIfAllPlayersReady(currentRoom.players)
          if (isGameCanStart) {
            currentRoom.isTimerStart = true;
            roomsMap.set(roomId, currentRoom);

            customSocket.sendToAllInRoom(roomId, 'REMOVE_ISREADY_BUTTON', [roomId]);
            startGame(roomId);
          }
        } else {
          roomsMap.delete(roomId);
          currentRoomsList = getCurrentRoomsList();
          customSocket.sendToAll('UPDATE_ROOMS', [currentRoomsList])
        }
      }
    }

    function deleteUser(currentRoom) {
      const deletedUser = currentRoom.players.map(player => {
        return player.name === username
          ? undefined
          : player
      })

      currentRoom.players = deletedUser.filter(player => player != undefined);
      return currentRoom
    }

    function setPlayerIsReady(roomId, username) {
      const currentRoom = roomsMap.get(roomId);
      currentRoom.players = currentRoom.players.map(player => {
        return player.name === username
          ? Object.assign(player, { isReady: true })
          : player
      })
      roomsMap.set(roomId, currentRoom);

      customSocket.sendOnlyToCurrentClient('UPDATE_USER_INFO', [currentRoom])
      customSocket.sendToAllInRoomExceptCurrentClient(roomId, 'UPDATE_ROOM', [currentRoom.players]);

      const isGameCanStart = currentRoom.players.length > 1 && checkIfAllPlayersReady(currentRoom.players)
      if (isGameCanStart) {
        currentRoom.isTimerStart = true;

        roomsMap.set(roomId, currentRoom);

        customSocket.sendToAllInRoom(roomId, 'REMOVE_ISREADY_BUTTON', [roomId]);

        startGame(roomId);
      }
    }

    function startGame(roomId) {
      let currentRoom = roomsMap.get(roomId);

      const bot = new Bot(currentRoom.players);

      customSocket.sendToAllInRoom(roomId, 'BOT_MESSAGE', [bot.getHelloMessage()]);

      if (currentRoom.textIndex == -1) {
        currentRoom.textIndex = Math.round(Math.random() * 6)
        customSocket.sendOnlyToCurrentClient('GET_TEXT_INDEX', [roomId, currentRoom.textIndex]);
        roomsMap.set(roomId, currentRoom);
      }

      let secondsBeforeStart = config.SECONDS_TIMER_BEFORE_START_GAME;

      customSocket.sendToAll('UPDATE_ROOMS', [currentRoomsList])

      setTimeout(() => {
        customSocket.sendToAllInRoom(roomId, 'BOT_MESSAGE', [bot.getPlayersAnnouncementMessage()]);

        const changeBeforeGameTimer = setInterval(() => {
          if (secondsBeforeStart > 0) {
            customSocket.sendToAllInRoom(roomId, 'CHANGE_TIMER_BEFORE_GAME', [secondsBeforeStart]);
            secondsBeforeStart--;
          } else {
            clearInterval(changeBeforeGameTimer);

            let secondsForGame = config.SECONDS_FOR_GAME;

            currentRoom = roomsMap.get(roomId);
            const currentPlayer = currentRoom.players.find(player => player.name === username);
            // console.log(currentPlayer);

            customSocket.sendToAllInRoom(roomId, 'SHOW_GAME_TEXT', [currentRoom.text.split(''), currentPlayer, roomId]);

            const sendBotMessages = setInterval(() => {
              customSocket.sendToAllInRoom(roomId, 'BOT_MESSAGE', [bot.getGameMessages(roomId, roomsMap)]);
            }, 2000);

            const setInfoAboutGame = setInterval(() => {
              let intervalCounter = 30;

              if (intervalCounter < config.SECONDS_FOR_GAME) {
                const players = currentRoom.players.sort(compareForGameStatus);
                let counter = 1

                for (let i = 0; i < players.length; i++) {
                  i === 0
                    ? currentRoom.systemMessagesQueue.push(`${counter++}. ${players[i].name}. ${currentRoom.text.length + 1 - players[i].currentWordIndex} symbols until the end`)
                    : currentRoom.systemMessagesQueue.push(`${counter++}. ${players[i].name}. ${currentRoom.text.length + 1 - players[i].currentWordIndex} symbols until the end
                  Distance to the previous player: ${players[i - 1].currentWordIndex - players[i].currentWordIndex}`)
                }
                roomsMap.set(roomId, currentRoom);
                intervalCounter += 30;
              }
            }, 30000)

            const changeGameTimer = setInterval(() => {
              if (secondsForGame > 0) {
                customSocket.sendToAllInRoom(roomId, 'CHANGE_GAME_TIMER', [secondsForGame]);
                secondsForGame--;

              } else {
                currentRoom.isFinished = true;

                roomsMap.set(roomId, currentRoom);

                clearInterval(setInfoAboutGame);
                clearInterval(sendBotMessages);
                clearInterval(changeGameTimer);

                customSocket.sendToAllInRoom(roomId, 'BOT_MESSAGE', [bot.getLeaderBoardMessage(currentRoom.players)]);
                setTimeout(() => {
                  currentRoom.players.forEach(player => {
                    player.isReady = false;
                    player.currentWordIndex = 0;
                    winnerTablePosition = 0;
                  })

                  const updatedRoom = Object.assign({}, currentRoom, {
                    text: [],
                    isTimerStart: false,
                    isFinished: false,
                    textIndex: -1,
                    systemMessagesQueue: []
                  })
                  customSocket.sendToAllInRoom(roomId, 'UPDATE_ROOM', [updatedRoom.players])
                  customSocket.sendToAllInRoom(roomId, 'SHOW_ISREADY_BUTTON', [roomId]);
                }, 3000)
              }
            }, 1000)
          }
        }, 1000)
      }, 1500)
    }

    function setPlayerIsNotReady(roomId, username) {
      const currentRoom = roomsMap.get(roomId);
      currentRoom.players = currentRoom.players.map(player => {
        return player.name === username
          ? Object.assign(player, { isReady: false })
          : player
      })

      roomsMap.set(roomId, currentRoom);

      customSocket.sendOnlyToCurrentClient('UPDATE_USER_INFO', [currentRoom]);
      customSocket.sendToAllInRoomExceptCurrentClient(roomId, 'UPDATE_ROOM', [currentRoom.players]);
    }

    function getTextByIdIsDone(roomId, text) {
      let currentRoom = roomsMap.get(roomId);
      currentRoom.text = text;
      roomsMap.set(roomId, currentRoom);
    }

    function checkInputLetter(inputKey, roomId) {
      let currentRoom = roomsMap.get(roomId);
      const currentPlayer = currentRoom.players.find(player => player.name === username);
      const textArray = currentRoom.text.split('')

      if (inputKey === textArray[currentPlayer.currentWordIndex]) {
        currentRoom.players = currentRoom.players.map(player => {
          if (player.name === username) {
            player.currentWordIndex++;
          }
          return player;
        })
        roomsMap.set(roomId, currentRoom);
        if (textArray[currentPlayer.currentWordIndex] === undefined) {
          currentRoom.players = currentRoom.players.map(player => {
            if (player.name === username) {
              player.currentWordIndex++;
              player.winnerTablePosition = ++winnerTablePosition;
            }
            return player;
          })
          roomsMap.set(roomId, currentRoom);
          checkIfAllPlayersFinished(currentRoom.players)
            ? extraFinishGame(roomId, currentRoom)
            : null
        } else if (textArray.length - currentPlayer.currentWordIndex === 30) {
          currentRoom.systemMessagesQueue.push(`${username} has 30 characters left until the end`);
          roomsMap.set(roomId, currentRoom);
        } else if (textArray.length - currentPlayer.currentWordIndex === 30) {
          currentRoom.systemMessagesQueue.push(`${username} near the finish`)
        }
        const updatedCurrentPlayer = currentRoom.players.find(player => player.name === username);

        customSocket.sendOnlyToCurrentClient('SHOW_GAME_TEXT', [currentRoom.text.split(''), updatedCurrentPlayer, roomId]);
        customSocket.sendToAllInRoomExceptCurrentClient(roomId, 'CHANGE_PROGRESS_INDICATOR', [currentRoom.text.split(''), updatedCurrentPlayer, roomId]);
        customSocket.sendOnlyToCurrentClient('CHANGE_PROGRESS_INDICATOR', [currentRoom.text.split(''), updatedCurrentPlayer, roomId]);
      }
      else {
        customSocket.sendOnlyToCurrentClient('SHOW_GAME_TEXT', [currentRoom.text.split(''), currentPlayer, roomId]);
      }
    }

    function extraFinishGame(roomId, currentRoom) {
      const bot = new Bot(currentRoom.players);

      currentRoom.isFinished = true;

      roomsMap.set(roomId, currentRoom);

      customSocket.sendToAllInRoom(roomId, 'BOT_MESSAGE', [bot.getLeaderBoardMessage(currentRoom.players)]);
      setTimeout(() => {
        currentRoom.players.forEach(player => {
          player.isReady = false;
          player.currentWordIndex = 0;
          winnerTablePosition = 0;
        })

        const updatedRoom = Object.assign({}, currentRoom, {
          text: [],
          isTimerStart: false,
          isFinished: false,
          textIndex: -1,
          systemMessagesQueue: []
        })
        customSocket.sendToAllInRoom(roomId, 'UPDATE_ROOM', [updatedRoom.players])
        customSocket.sendToAllInRoom(roomId, 'SHOW_ISREADY_BUTTON', [roomId]);
      }, 3000)
    }

    function extraLeaveRoom() {
      const currentRoomsList = getCurrentRoomsList();
      customSocket.sendToAllClientsExceptCurrentClient('UPDATE_ROOMS', [currentRoomsList]);
    }

    socket.on('disconnect', () => {
      if (currentRoom) {
        roomsMap.set(currentRoom.name, deleteUser(currentRoom));

        customSocket.leave(currentRoom.name, extraLeaveRoom);

        const isGameCanStart = currentRoom.players.length > 1 && checkIfAllPlayersReady(currentRoom.players)
        if (isGameCanStart) {
          currentRoom.isTimerStart = true;
          roomsMap.set(currentRoom.name, currentRoom);
          customSocket.sendToAllInRoom(currentRoom.name, 'REMOVE_ISREADY_BUTTON', [currentRoom.name]);
          startGame(currentRoom.name);
        }
        customSocket.sendToAllInRoom(currentRoom.name, 'UPDATE_ROOM', [currentRoom.players]);
      }
    });
  });
};
